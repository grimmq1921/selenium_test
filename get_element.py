import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager 

options = webdriver.ChromeOptions() 
options.add_experimental_option("excludeSwitches", ["enable-logging"])

driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()),options=options)
driver.implicitly_wait(10)
url = "https://www.google.com.tw/"



def open_google_page():
    driver.get(url)
    get_title = driver.title
    driver.maximize_window()

    try: 
        assert get_title == 'Google'
        print('pass title')
    except Exception as e :
        print('Fail title',e)

    driver.find_element(By.XPATH,"//input[@class='gLFyf' and @name='q']").send_keys('天氣')
    driver.find_element(By.XPATH,"//input[@class='gLFyf']").send_keys(Keys.ENTER)


def search_keyword():
    driver.find_elements(By.XPATH,"//h3[@class='LC20lb MBeuO DKV0Md']")[0].click()
    time.sleep(1)
    driver.find_element(By.XPATH,"//a[@id='btnDay1']").click()

    time.sleep(1)
    date = driver.find_element(By.XPATH,"//span[@id='fcst_time']").text
    print(f"日期: {date}")


    for i in range(22):
        
        city_name = driver.find_elements(By.CLASS_NAME,"city")[22+i].text
        city_rain = driver.find_elements(By.XPATH,"//span[@class='rain']")[i].text

        print(f'都市名稱:{city_name}  降雨機率:{city_rain}')
    
    time.sleep(2)

    driver.save_screenshot("今日天氣.png")
    driver.quit()